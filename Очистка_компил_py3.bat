
@echo ver 2020.05.13 for Win10 and python 3.8 + 2.7

@echo off
@color 3E
rem @color 9A


:: --- delete directories ------------------------------------------------------
@echo.
@echo.
@echo --- Delete Directories ---
@echo.

REM del /Q tempfoldersempty.txt
for /r %cd% %%F in (.) do (
    REM if "%%~nxF"=="__pycache__" echo "%%~npF" >> tempfoldersempty.txt
    if "%%~nxF"=="__pycache__" rd /S /Q "%%~npF"
)

REM rmdir /Q logs


:: ----delete files----------
@echo.
@echo ---Delete Files---
@echo.

del /F /S /Q *.pyc
REM del /F /S /Q logs\*.*

@echo.
@echo.
@echo ---�� 䠩�� 㤠����!---
@sleep 5
@exit
@pause
