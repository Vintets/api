#!/home/admin/venv_flask38/bin/python3
# -*- coding: utf-8 -*-

if 'import':
    import os.path
    import sys
    import time
    import json
    cur_script = __file__
    py34 = sys.version_info >= (3, 4, 0)
    if sys.platform == 'win32' and not py34:
        cur_script = unicode(cur_script, 'cp1251')
    PATH_SCRIPT = os.path.abspath(os.path.dirname(cur_script))
    os.chdir(os.path.dirname(PATH_SCRIPT))
    sys.path.insert(0, os.path.dirname(PATH_SCRIPT))
    import accessory.colorprint as cp
    import accessory.clear_consol as cc
    import accessory.authorship as auth_sh
    from accessory.ini import Ini

    from collections import defaultdict
    import smtplib
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    # from email.mime.base import MIMEBase
    # from email.utils import formatdate
    # if py34:
        # from email import encoders as Encoders
    # else:
        # from email import Encoders
    import logging
    import my_logger
    err_logger = logging.getLogger('error_logger')


class MyErrorsMailer():
    def errors(self, _type, arg=u''):
        if _type == 'noOpen':
            err = u'13Ошибка открытия файла: %s' % arg
            errlg = u'13Ошибка открытия файла: ^15_%s' % arg
        elif _type == 'noSender_Name':
            err = errlg = u'13Не задан отправитель'
        elif _type == 'noLoginPass':
            err = errlg = u'13Не задан логин-пароль отправителя'
        elif _type == 'noToaddrFile':
            err = errlg = u'13Не задано имя файла с адресатами'
        elif _type == 'noSend':
            err = errlg = u'13Ошибка отправки e-mail'
        cp.cprint(errlg)
        cp.cprint(u'14---------------   ^12_ERROR   ^14_---------------')
        err_logger.error(err[2:])
        # exit(1)
        self.error = True


class Mailer(MyErrorsMailer):
    def __init__(self, path_config=u'', email_sender=None, toaddr=u'', msg_print=False):
        self.path_config        = path_config
        self.msg_print          = msg_print
        self.email_sender       = email_sender
        self.username           = None
        self.password           = None
        self.error              = False
        self.get_login_pass()
        self.fromaddr           = u'Time - teplonet.ru <%s>' % self.username
        self.toaddr             = self.str2list(toaddr)
        self.toaddr_test        = [u'kvv <kvv@domtepla.ru>'] #, u'vintets <vintets@mail.ru>']

    def get_login_pass(self):
        if self.email_sender is None:
            self.errors('noSender_Name')
            return
        ini = Ini(os.path.join(self.path_config, u'auth_code.ini'))
        if ini.set_name_section(self.email_sender):
            self.username = ini.get_param('login')
            self.password = ini.get_param('pass')
            if self.username and self.password: return
        self.errors('noLoginPass')

    def str2list(self, s):
        return list(map(lambda x: x.strip(), s.split(';')))

    def read_toaddr_email_file(self, toaddr_file):
        if not toaddr_file:
            self.errors('noToaddrFile')
            return False
        filename = os.path.join(self.path_config, toaddr_file)
        self.check_read_file(filename)
        if self.error: return False
        with open(filename, 'r') as fr:
            text = fr.read()
            if not py34:
                text = text.decode('utf-8')
        self.toaddr = self.str2list(text)

    def check_read_file(self, file):
        if not os.path.isfile(file):
            self.errors('noOpen', file)

    def send_email(self, subject, msg_txt, test=False):
        toaddr = self.toaddr
        if test:
            toaddr = self.toaddr_test

        # Заголовок         subject
        # Текст сообщения   msg_txt

        # Compose attachment
        # basename = os.path.basename(self.filename_out)
        # part = MIMEBase('application', 'octet-stream')
        # part.set_payload(open(self.filename_out, 'rb').read())
        # Encoders.encode_base64(part)
        # part.add_header('Content-Disposition', 'attachment; filename="%s"' % basename)

        # Compose message
        if self.msg_print:
            # print(json.dumps(toaddr, indent=4, ensure_ascii=False))
            print(u'\t%s' % u',\n\t'.join(toaddr))

        msg = MIMEMultipart()
        msg['From']     = self.fromaddr
        msg['To']       = ','.join(toaddr)
        # msg['CC']       = ', '.join(cc_emails)
        # msg['BCC']       = ', '.join(bcc_emails)
        msg['Subject']  = subject
        # msg['charset']  = 'utf-8'
        msg.attach(MIMEText(msg_txt, 'html', 'utf-8')) # plain/html
        # msg.attach(part)

        # Send mail
        smtp = smtplib.SMTP_SSL(host='smtp.yandex.ru', port=465)
        # smtp.connect('smtp.yandex.ru', 465)
        # smtp.set_debuglevel(1);  # Выводим на консоль лог работы с сервером
        smtp.login(self.username, self.password)
        smtp.sendmail(self.fromaddr, toaddr, msg.as_string())
        smtp.quit()
        return toaddr

    def send(self, subject, msg_txt, test=False):
        if self.error:
            return False
        if self.msg_print:
            cp.cprint(u'1Отправляем e-mail')
        try:
            sended_address = self.send_email(subject, msg_txt, test=test)
        except Exception:
            self.errors('noSend')
            return False
        if self.msg_print:
            cp.cprint(u'2Письмо успешно отправлено')
        return True


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ------------------------------------------------------------------------------
# ==============================================================================


if __name__ == '__main__':
    _width = 100
    _hight = 50
    if sys.platform == 'win32':
        os.system('color 71')
        os.system('mode con cols=%d lines=%d' % (_width, _hight))
    # os.chdir(PATH_SCRIPT)
    cc.clearConsol()

    __author__ = u'master by Vint'
    __title__ = u'--- mailer ---'
    __version__ = u'0.3.1'
    __copyright__ = u'Copyright 2019 (c)  bitbucket.org/Vintets'
    auth_sh.authorship(__author__, __title__, __version__, __copyright__, width=_width)


    email_sender    = u'email_robot_teplonet'
    # toaddr_file     = u'_email_send_list_montag.txt'
    subject         = u'Тест отправки e-mail'
    html            = u'Тест Тест Тест'

    mailer = Mailer(
            path_config=u'configs',
            email_sender=email_sender,
            toaddr=u'',
            msg_print=True
            )
    # mailer.read_toaddr_email_file(toaddr_file)
    print(u'%s %s %s' % (mailer.username, mailer.password, mailer.toaddr))
    status = mailer.send(subject, html, test=True)
    if not status:
        mailer.errors('noSend')

    # time.sleep(1)
    # if PY34:
        # input(u'\n---------------   END   ---------------')
    # else:
        # raw_input(u'\n---------------   END   ---------------')
    print(u'\n---------------   END   ---------------')

