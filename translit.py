#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import string
import accessory.authorship as auth_sh

TRANSLIT_URL_DICT = {
        u'а': u'a',
        u'б': u'b',
        u'в': u'v',
        u'г': u'g',
        u'д': u'd',
        u'е': u'e',
        u'ё': u'yo',
        u'ж': u'zh',
        u'з': u'z',
        u'и': u'i',
        u'й': u'j',
        u'к': u'k',
        u'л': u'l',
        u'м': u'm',
        u'н': u'n',
        u'о': u'o',
        u'п': u'p',
        u'р': u'r',
        u'с': u's',
        u'т': u't',
        u'у': u'u',
        u'ф': u'f',
        u'ц': u'c',
        u'ч': u'ch',
        u'ш': u'sh',
        u'щ': u'shch',
        u'ы': u'y',
        u'э': u'e',
        u'ю': u'yu',
        u'я': u'ya'
        }


def translit(text):
    _translit_list = []
    _prev = u''
    for symbol in text:
        # print(u'Символ', symbol, 'capital =', symbol.isupper())
        # не буквы
        if (not symbol.isalpha()) or (symbol in string.ascii_letters):
            _prev = u''
            _translit_list.append(symbol)
            continue
        # ь и ъ знаки
        if symbol in (u'ъ', u'ь'):
            _prev = u''
            continue

        # основные замены
        capital = symbol.isupper()
        tr_sym = TRANSLIT_URL_DICT.get(symbol.lower(), None)
        if symbol.lower() == u'х':  # буква 'х'
            tr_sym = u'kh' if _prev in u'cseh' else u'h'
        if tr_sym is not None:
            _prev = tr_sym[-1]
            if capital:
                tr_sym = tr_sym.capitalize()
            _translit_list.append(tr_sym)
            continue

        # print(u'Символ - непонятная хрень')
        _prev = u''
    return u''.join(_translit_list)


if __name__ == '__main__':
    _width = 100
    _hight = 50
    if sys.platform == 'win32':
        os.system('color 71')
        # os.system('mode con cols=%d lines=%d' % (_width, _hight))

    __author__ = u'master by Vint'
    __title__ = u'--- translit Yandex ---'
    __version__ = u'0.1.3'
    __copyright__ = u'Copyright 2019 (c)  bitbucket.org/Vintets'
    auth_sh.authorship(__author__, __title__, __version__, __copyright__)

    _t1 = u'Я уже устал повторять, зачем вообще это нужно. В следующий раз... Test!?&-_()"'
    _t2 = u'цхенвал схема мех шхуна бахча кх зх цх сх ех жх'
    _t3 = u'Ё\tй\tц\tщ\tъыь\tэ\tю\tя'
    _t4 = u'кх\tзх\tцх\tсх\tех\tжх'
    print('%s\n%s\n' % (_t1, translit(_t1)))
    print('%s\n%s\n' % (_t2, translit(_t2)))
    print('%s\n%s\n' % (_t3, translit(_t3)))
    print('%s\n%s\n' % (_t4, translit(_t4)))

    try:
        input = raw_input
    except NameError:
        pass
    input('---------------   END   ---------------')


'''
u'Ё й ц щ  ъыь э ю я  кх зх цх сх ех жх'

http://www.translityandex.ru/
yo-y-c-shch--y-e-yu-ya--kh-zh-ch-sh-eh-zhh

http://translit-online.ru/yandex.html
yo-j-c-shch-y-eh-yu-ya-kkh-zkh-ckh-skh-ekh-zhkh

https://translitonline.com/dlya-seo/
yo-j-c-shch-y-e-yu-ya-kh-zh-ckh-skh-ekh-zhkh
'''
